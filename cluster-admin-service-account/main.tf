data "google_client_config" "default" {}

provider "kubernetes" {
}

variable "sa_name" {
  type        = string
  default     = "ci-admin"
  description = "Name of the admin service account being created."
}

variable "sa_namespace" {
  type        = string
  default     = "kube-system"
  description = "Namespace of the service account."
}

output "token" {
  value = lookup(data.kubernetes_secret.ci.data, "token")
  sensitive = true
}

output "ca_crt" {
  value = lookup(data.kubernetes_secret.ci.data, "ca.crt")
  sensitive = true
}

resource "kubernetes_service_account" "ci" {
  metadata {
    name = var.sa_name
    namespace = var.sa_namespace
  }
}

data "kubernetes_secret" "ci" {
  metadata {
    name = kubernetes_service_account.ci.default_secret_name
  }
}

# make the assign the cluster-admin role to the "ci" service account
# the cluster-admin ClusterRole exists by default and allows superuser operations
# https://kubernetes.io/docs/reference/access-authn-authz/rbac/#default-roles-and-role-bindings
resource "kubernetes_cluster_role_binding" "ci-admin" {
  metadata {
    name = var.sa_name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = var.sa_name
    namespace = var.sa_namespace
  }
}



